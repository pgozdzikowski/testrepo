package pl.atende.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class SimpleTestClass {
	public static void main(String[] args) {
		System.out.println("Hello world !!");
		List<String> list = new LinkedList<>();
	}
	
	public void printFile(String fileName) {
		try(BufferedReader buf = new BufferedReader(new FileReader(fileName))) {
			String line;
			while((line = buf.readLine()) != null) {
				System.out.println(line);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("This is simple line");
		System.out.println("This is simple line2");
		System.out.println("This is simple line3");
		System.out.println("This is simple line4");
	}
	
}
